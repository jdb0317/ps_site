// server.js
let express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    serveStatic = require('serve-static'),
    nodeMailer = require('nodemailer'),
    constantsJson = require('./src/constants.json');

app = express();
app.use(serveStatic(__dirname + "/dist"));
//app.use(express.static('src'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 5000;

app.post('/send-email', function (req, res) {

    //massage data
    let authUser = "jonathanbrown0123@gmail.com",
        authPass = "Food4me2",
        mailRecipient = "jonathanbrown0123@gmail.com";
    
    let userContactInfo = "";
    if(req.body.phone.trim() != "" && req.body.email.trim() != "") {
        userContactInfo = `${req.body.phone}<br />${req.body.email}`;
    }
    else if(req.body.phone === "") {
        userContactInfo = req.body.email;
    }
    else if(req.body.email === "") {
        userContactInfo = req.body.phone;
    }

    let serviceData = constantsJson.services.filter(function (s) { return s.id === Number(req.body.category); });
    let serviceTitle = serviceData.length < 1 ? "Mensagem Geral" : serviceData[0].title;

    //send email
    let transporter = nodeMailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: { user: authUser, pass: authPass }
    });

    let mailOptions = {
        to: mailRecipient,
        subject: "Requisição de contato",
        html: `<h2>Requisição de contato</h2>
            <b>Serviço: </b>${serviceTitle}<br />
            <b>Nome: </b>${req.body.name}<br />
            <b>Contato: </b>${userContactInfo}<br /><br />
            ${req.body.message}`
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
    
    
    res.writeHead(200);
    res.end();
});

app.listen(port);
console.log('server started '+ port);
