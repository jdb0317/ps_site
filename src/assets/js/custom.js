$(function() {

$("ul.anchor-link").click(function(e){
	e.preventDefault();
	var targetHref = $(e.target).attr("href");
	var target = $(targetHref);
	$('html,body').animate({scrollTop: target.offset().top},'slow');
});

$("#contactus [type='submit']").click(function(e) {
	e.preventDefault();
	var msgWrapper = $("#contactus_msg");
	var msgList = $("#contactus_msg ul");

	$(msgList).empty();
	$(msgWrapper).show();
	$(msgWrapper).removeClass("error").removeClass("success");

	var errors = validateForm();
	if(errors.length > 0) 
	{
		$(msgWrapper).addClass("error");
		errors.forEach(msg => {
			$(msgList).append("<li>" + msg + "</li>");	
		});
	}
	else
	{
		$(msgWrapper).addClass("success");
		$(msgList).append("<li>Mensagem enviada. Entraremos em contato em breve.</li>");
	}

	var scrollTotarget = $("#contactus_msg");
	$('html,body').animate({scrollTop: scrollTotarget.offset().top - scrollTotarget.height()},'fast');
	
});

$("#contactus [type='reset']").click(function() {
	$("#contactus_msg").hide();
	$("#contactus_msg").removeClass("error").removeClass("success");
	$("#contactus_msg ul").empty();
	$("#contact_name").removeClass("error");
	$("#contact_email").removeClass("error");
	$("#contact_phone").removeClass("error");
	$("#contact_message").removeClass("error");
});

function validateForm() {
	
	var errorMessage = [];
	var name = String($("#contact_name").val()).trim();
	var email = String($("#contact_email").val()).trim();
	var phone = String($("#contact_phone").val()).trim();
	var message = String($("#contact_message").val()).trim();

	$("#contact_name").removeClass("error");
	$("#contact_email").removeClass("error");
	$("#contact_phone").removeClass("error");
	$("#contact_message").removeClass("error");

	//Validate name
	if (name === '') {
		errorMessage.push("Insira um nome");
		$("#contact_name").addClass("error");
	}
	else if (/^[a-zA-Z\s]*$/.test(name) == false) {
		errorMessage.push("Somente letras permitidas no nome");
		$("#contact_name").addClass("error");
	}

	//Validate email and/or phone (not actually validating phone number)
	var re = /^(([^<>()\\.,;:\s@"]+(\.[^<>()\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (email === '' && phone === '') {
		errorMessage.push("Insira um email ou número de telefone");
		$("#contact_email").addClass("error");
		$("#contact_phone").addClass("error");
	}
    else if (re.test(email) == false) {
		errorMessage.push("O email está em formato incorreto");
		$("#contact_email").addClass("error");
	}

	//Validate that message isn't blank
	if (message === '') {
		errorMessage.push("Digite uma mensagem");
		$("#contact_message").addClass("error");
	}


	return errorMessage;
}
});